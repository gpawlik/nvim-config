require("gitlinker").setup()

require("which-key").register({
  ["<leader>g"] = { name = "[G]it{hub,lab}", _ = "which_key_ignore" },
  ["<leader>gy"] = { name = "[Y]ank permalink", _ = "which_key_ignore" }, -- comes as default
  -- ["<leader>gb"] = { name = "open in [B]rowser", _ = "which_key_ignore" },
})
vim.keymap.set(
  "n",
  "<leader>gb",
  '<cmd>lua require"gitlinker".get_buf_range_url("n", {action_callback = require"gitlinker.actions".open_in_browser})<cr>',
  -- '<cmd>lua require"gitlinker".get_repo_url({action_callback = require"gitlinker.actions".open_in_browser})<cr>',
  { desc = "open permalink in [B]rowser" }
)
vim.api.nvim_set_keymap(
  "v",
  "<leader>gb",
  '<cmd>lua require"gitlinker".get_buf_range_url("v", {action_callback = require"gitlinker.actions".open_in_browser})<cr>',
  { desc = "open selection in [B]rowser" }
)
