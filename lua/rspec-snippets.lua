local function toggle_rspec_focus()
  -- Save the current cursor position
  local current_pos = vim.api.nvim_win_get_cursor(0)

  -- Find the line with 'RSpec.describe' or 'RSpec.fdescribe'
  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, false)
  local describe_line = nil
  for i, line in ipairs(lines) do
    if line:match("RSpec%.describe") then
      describe_line = i
      break
    elseif line:match("RSpec%.fdescribe") then
      describe_line = i
      break
    end
  end

  -- If 'RSpec.describe' is found, replace it with 'RSpec.fdescribe'
  -- If 'RSpec.fdescribe' is found, replace it with 'RSpec.describe'
  if describe_line then
    local new_line = lines[describe_line]
    if new_line:match("RSpec%.describe") then
      new_line = new_line:gsub("RSpec%.describe", "RSpec.fdescribe")
    else
      new_line = new_line:gsub("RSpec%.fdescribe", "RSpec.describe")
    end
    vim.api.nvim_buf_set_lines(0, describe_line - 1, describe_line, false, { new_line })

    -- Restore the cursor position
    vim.api.nvim_win_set_cursor(0, current_pos)
  else
    -- No 'RSpec.describe' or 'RSpec.fdescribe' found
    vim.api.nvim_win_set_cursor(0, current_pos)
  end
end

require("which-key").register({
  ["<leader>x"] = { name = "[X] Snippets", _ = "which_key_ignore" },
  ["<leader>xr"] = { name = "[R]uby", _ = "which_key_ignore" },
})

vim.keymap.set("n", "<leader>xrf", toggle_rspec_focus, { desc = "Focus [R]Spec [F]ocus this file" })
